<?php if (!defined('THINK_PATH')) exit();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">



<html xmlns="http://www.w3.org/1999/xhtml">



<head>



    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />



    <title>Continuous Inkjet Printer,Pneumatic Coder,DOD Inkjet Printer</title>



    <meta content="Small Character Ink Jet Printer, Pneumatic Coder,DOD Inkjet Printer" name="keywords" />



    <meta content="Raymond inkjet printer can be used in a variety of metals, fonts , logos etc and widely applied to various industrial, food ,beverage etc.." name="description" />



    <base href="" />



    <link href="/Public/Home/css/style.css" rel="stylesheet" type="text/css" />

    <link href="/Public/Home/css/jquery.jslides.css" rel="stylesheet" type="text/css" />

    <!-- <link href="/Public/Home/css/demo.css" rel="stylesheet" type="text/css" /> -->

    <!-- <link href="/Public/Home/css/validform-style.css" rel="stylesheet" type="text/css" /> -->

    <!-- <script type="text/javascript" src="/Public/Home/js/jquery.min.js"></script> -->

    <script type="text/javascript" src="/Public/Home/js/jquery-1.8.0.min.js"></script>

    <script type="text/javascript" src="/Public/Home/js/jquery.jslides.js"></script>

    <script type="text/javascript" src="/Public/Home/js/jquery.SuperSlide.2.1.1.js"></script>

    <script type="text/javascript" src="/Public/Home/js/Validform_v5.3.2.js"></script>
    <!-- Smartsupp Live Chat script -->
<!-- <script type="text/javascript">
    var _smartsupp = _smartsupp || {};
    _smartsupp.key = 'befa92e2ed221f203d17940b0944cd7a6b69b113';
    window.smartsupp||(function(d) {
      var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
      s=d.getElementsByTagName('script')[0];c=d.createElement('script');
      c.type='text/javascript';c.charset='utf-8';c.async=true;
      c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
    })(document);
    </script> -->
    

</head>

<body>

    <!--顶部 -->
    
    <div class="top">
    
        <div class="wrap">
    
    
            <!--LOGO -->
    
    
            <div class="logo"><a href="http://www.whraymond.com/"><img src="/Public/Home/img/logo.png"/></a></div>
    
    
            <!--LOGO -->
    
    
            <div class="daohang">
                <ul>
                    <li><a href="/" <?php if(CONTROLLER_NAME == 'Index'): ?>class="now"<?php endif; ?>>Home</a></li>
                    <li><a href="/Industries.html" <?php if(CONTROLLER_NAME == 'Industries'): ?>class="now"<?php endif; ?>>Industries</a></li>
                    <li><a href="/Products.html" <?php if(CONTROLLER_NAME == 'Products'): ?>class="now"<?php endif; ?>>Products</a></li>
                    <li><a href="/Technology.html" <?php if(CONTROLLER_NAME == 'Technology'): ?>class="now"<?php endif; ?>>Technology Guide</a></li>
                    <li><a href="/Support.html" <?php if(CONTROLLER_NAME == 'Support'): ?>class="now"<?php endif; ?>>Support</a></li>
                    <li><a href="/Distributors.html" <?php if(CONTROLLER_NAME == 'Distributors'): ?>class="now"<?php endif; ?>>Distributors</a></li>
                    <li><a href="/News.html" <?php if(CONTROLLER_NAME == 'News'): ?>class="now"<?php endif; ?>>News</a></li>
                    <li><a href="/About.html" <?php if(CONTROLLER_NAME == 'About'): ?>class="now"<?php endif; ?>>About</a></li>
                </ul>
            </div>
    
    
        </div>
    
    
    </div>
    
    
    <!--顶部 -->
    <style>#full-screen-slider {
        margin-top: 30px;
        margin-bottom: 30px;
    }</style>
    
    




</div>

<!--顶部 --><!--位置 -->
<div class="weizhi wrap"><a href="http://www.whraymond.com/">Home</a> > <a href="list.php?catid=70">Products</a></div>
<!--位置 -->

<!--盒子 -->
<div class="wrap">

<!--左侧 -->
<div class="nav_left">

<div class="dep_title"><h3>Products</h3></div>

<ul>
  <?php if(is_array($products)): $i = 0; $__LIST__ = $products;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="Products/<?php echo (str_replace(' ','-',$vo["name"])); ?>.html"><?php echo ($vo["name"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?> 
</ul>

</div>
<!--左侧 -->

<!--中间 -->
<div class="nav_center">

<div class="dep_bt"><h4>Products</h4></div>

<style>
#full-screen-slider { width:700px; height:233px; float:left; position:relative}
#slides { display:block; width:700px;  height:233px; list-style:none; padding:0; margin:0; position:relative}
#slides li { display:block; width:700px; height:100%; list-style:none; padding:0; margin:0; position:absolute}
#slides li a { display:block; width:700px;  height:100%; text-indent:-9999px}
#pagination { display:none;}
</style>


<div style="clear:both"></div>

<!--产品类别 -->
<div class="cp_list">
<ul>
  <?php if(is_array($products)): $i = 0; $__LIST__ = $products;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li>
    <a href="/Products/<?php echo (str_replace(' ','-',$vo["name"])); ?>.html"><img src="<?php echo ($vo['pic']); ?>" alt="<?php echo ($vo["name"]); ?>" /></a>
    <span><a href="/Products/<?php echo (str_replace(' ','-',$vo["name"])); ?>.html"><?php echo ($vo["name"]); ?></a></span>
    </li><?php endforeach; endif; else: echo "" ;endif; ?>
    <!-- <li>
    <a href="/list.php?catid=72"><img src="/Public/upload/20151227125543515.jpg" alt="DOD Inkjet Printer" /></a>
    <span><a href="/list.php?catid=72">DOD Inkjet Printer</a></span>
    </li>

    <li>
    <a href="/list.php?catid=73"><img src="/Public/upload/20151227125818142.jpg" alt="Thermal Inkjet Printer" /></a>
    <span><a href="/list.php?catid=73">Thermal Inkjet Printer</a></span>
    </li>

    <li>
    <a href="/list.php?catid=76"><img src="/Public/upload/20151227125458941.jpg" alt="Laser Coder" /></a>
    <span><a href="/list.php?catid=76">Laser Coder</a></span>
    </li>

    <li>
    <a href="/list.php?catid=77"><img src="/Public/upload/20151227125448917.jpg" alt="Pneumatic Coder" /></a>
    <span><a href="/list.php?catid=77">Pneumatic Coder</a></span>
    </li>

    <li>
    <a href="/list.php?catid=82"><img src="/Public/upload/20151227125420259.jpg" alt="Consumable" /></a>
    <span><a href="/list.php?catid=82">Consumable</a></span>
    </li>

    <li>
    <a href="/list.php?catid=83"><img src="/Public/upload/20151227125332114.jpg" alt="Accessories" /></a>
    <span><a href="/list.php?catid=83">Accessories</a></span>
    </li> -->

</ul>
</div>
<!--产品类别 -->


</div>
<!--中间 -->





<!--右侧 -->
<div class="hezi_righr">

        <!--标题 -->
        <div class="r_bt">Contact US</div>
        <!--标题 -->

        <!--联系 -->
        <div class="lianxi">
            <span class="lx_tel">+86-27-86659195</span>
            <span class="lx_nr">
                <p>TEL：+86-27-86659195</p>
                <p>FAX：+86-27-86698193</p>
                <p>ADD：No 112,Guanggu Ave,Wuhan,China.Post code:430205</p>
                <p>MAIL：info@whraymond.com</p>
                <p>Contact：Andrew Yang</p>
            </span>

            <span class="lx_bot">
                <p class="sales">
                    <a href="/About/Contact-Sales.html">Contact Sales</a>
                </p>
            </span>


        </div>
        <!--联系 -->

    </div>
    <!--右侧 -->

</div>
<!--盒子 -->

<div style="clear:both"></div>

<div class="dibu">

    <ul class="wrap">

        <li>

            <dl>

                <dt>Coding Solutions</dt>
        
                <dd><a href="/Industries.html">Coated Metal</a></dd>

                <dd><a href="/Industries/Food.html" >Food</a></dd>

                <dd><a href="/Industries/Glass.html" >Glass</a></dd>

                <dd><a href="/Industries/Metal.html" >Metal</a></dd>

                <dd><a href="/Industries/Paper-and-card.html" >Paper and card</a></dd>

                <dd><a href="/Industries/Plastic.html" >Plastic</a></dd>

                <dd><a href="/Industries/Rubber.html" >Rubber</a></dd>

            </dl>

        </li>



        <li>

            <dl>

                <dt>Products</dt>

                <dd><a href="/Products.html">Continuous Inkjet Printer</a></dd>

                <dd><a href="/Products/DOD-Inkjet-Printer.html">DOD Inkjet Printer</a></dd>

                <dd><a href="/Products/Thermal-Inkjet-Printer.html">Thermal Inkjet Printer</a></dd>

                <dd><a href="/Products/Thermal-Transfer-Over-Printer.html">Thermal Transfer Over Printer</a></dd>

                <!--<dd><a href="/list.php?catid=75">Ink Roll Coder</a></dd>-->

                <dd><a href="/Products/Laser-Coder.html">Laser Coder</a></dd>

                <dd><a href="/Products/Pneumatic-Coder.html">Pneumatic Coder</a></dd>

                <dd><a href="/Products/Consumable.html">Consumable</a></dd>

                <dd><a href="/Products/Accessories.html">Accessories</a></dd>

            </dl>

        </li>



        <li>

            <dl>

                <dt>Technology Guide</dt>

                <!--<dd><a href="show.php?contentid=51">TTO</a></dd>-->

                <dd><a href="/Technology.html">Laser Coders</a></dd>

                <dd><a href="/Technology/TIJ.html">TIJ</a></dd>

                <dd><a href="/Technology/CIJ.html">CIJ</a></dd>

                <dd><a href="/Technology/DOD.html">DOD</a></dd>

                <dd><a href="/Technology/Pneumatic-marking-machine.html">Pneumatic marking machine</a></dd>



            </dl>

        </li>





        <li>

            <dl>

                <dt>Company</dt>

                <dd><a href="/About.html">About</a></dd>

                <dd><a href="/About/Event.html" >Event</a></dd>

                <dd><a href="/About/Certificate.html" >Certificate</a></dd>

                <dd><a href="/News.html">News</a></dd>

            </dl>

        </li>



    </ul>

</div>

<!--底部 -->



<!--底部 -->

<div class="footer">

    <div class="wrap">

<span class="footer_left">

<script type="text/javascript">

var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");

document.write(unescape("%3Cspan id='cnzz_stat_icon_1257578088'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s95.cnzz.com/z_stat.php%3Fid%3D1257578088%26show%3Dpic' type='text/javascript'%3E%3C/script%3E"));

</script>

<script>

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');



  ga('create', 'UA-74439664-1', 'auto');

  ga('send', 'pageview');



</script>

<p>
	Raymond Technology. All Rights Reserved.
</p></span>

        <span class="footer_right">

  <a href="https://www.youtube.com/channel/UCW1Ga2uRhC6GwSEHG4HJdOA"><img src="/Public/Home/img/05.jpg" /></a>

  <a href="https://plus.google.com/u/0/116533398269216120730"><img src="/Public/Home/img/01.jpg" /></a>

  <a href="https://www.facebook.com/injekpriint.raymond"><img src="/Public/Home/img/02.jpg" /></a>

  <a href="https://twitter.com/raymondinkjet"><img src="/Public/Home/img/03.jpg" /></a>

  <a href="http://www.linkedin.com/in/injekprinter-raymond-7b8a5a114?trk=nav_responsive_tab_profile_pic"><img src="/Public/Home/img/04.jpg" /></a>

</span>

    </div>

</div>

<!--底部 -->



</body>

</html>