<?php
namespace Home\Controller;
use Think\Controller;
class AboutController extends Controller {
    public function index(){
        $about=M('about')->select();
        $this->assign('about',$about);
        $this->display('index');
    }

    public function read($name){
        $name=str_replace(array("-","_")," ",$name);
        $map['name']=['eq',$name];
        $rel=M('about')->where($map)->find();
        $this->assign('rel',$rel);
        $about=M('about')->select();
        $this->assign('about',$about);
        $this->assign('name',$name);
        $this->display('read');
    }


}