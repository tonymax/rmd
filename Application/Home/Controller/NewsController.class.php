<?php
namespace Home\Controller;
use Think\Controller;
class NewsController extends Controller {
    public function index(){
        $news=M("news")->select();
        $this->assign('news',$news);
        //dump($news);
        $this->display('index');
        
    }

    public function read($title){
        $title=str_replace(array("-","_",'#')," ",$title);
        $where['tilte']=['eq',$title];

        $new=M('news')->where($where)->find();
        $this->assign('new',$new);
        $this->display();

    }

    public function show($cat_name,$name){
        //dump($cat_name);
        //dump($name);
        //die;
        $cat_name=str_replace(array("-","_",'#')," ",$cat_name);
        $name=str_replace(array("-","_")," ",$name);
        $where['name']=['eq',$name];
        
        $products=M('products')->select();
        $good=M('goods')->where($where)->find();
        $map['cat_name']=['eq',$cat_name];
        $goods=M('goods')->where($map)->select();
        //dump($products);
        //dump($goods);
        //产品分类
        $this->assign('products',$products);
        //某一个产品
        $this->assign('good',$good);
        $pics=M('pics')->where('gid='.$good['id'])->find();
        //dump($pics);
        $pics=explode(';',substr($pics['pics'],0,-1));
        //dump($pics);
        //die;
        $this->assign('pics',$pics);
        
        //某个分类下的产品
        $this->assign('goods',$goods);
        $this->display('show');

    }


}