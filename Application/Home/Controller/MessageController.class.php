<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Controller;
use Think\Controller;

/**
 * 后台首页控制器
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
class MessageController extends Controller {

    public function index(){
        $message=M('Message')->select();
        $this->assign('message',$message);
        $this->display();
    }

    public function add(){
        if(IS_POST){
            dump(I('post.'));
            //die;
            $data=$_POST['info'];
            $data['create_time']=time();
            dump($data);
            $rel=M('message')->create($data);
            if($rel){
                M('message')->add($data);
                dump(M('message')->getlastsql());
                echo 'success';
                //$this->success('提交成功!');
            }else{
				
                $this->error('提交失败!');
            }
        }else{
            $this->display();
        }
    }

}
