<?php
namespace Home\Controller;
use Think\Controller;
class IndustriesController extends Controller {
    public function index(){
        $industries=M('industries');
        $map['name']  = array('neq','Coated Metal');
        $rel=$industries->where($map)->select();
        $action=ACTION_NAME;
        
        $this->assign('rel',$rel);
        //dump($rel);
        // die;
        $this->assign('action',$action);
        $this->display('index');
    }

    public function read($name){
        $name=str_replace(array("-","_")," ",$name);
        //dump($name);
        $map['name']=array('eq',$name);
        $ind=M('industries')->where($map)->select();
        $rel=M('industries')->select();
        $this->assign('rel',$rel);
        $this->assign('ind',$ind);
        //dump($ind);
        $this->assign('name',$name);
        $this->display('read');
    }


}