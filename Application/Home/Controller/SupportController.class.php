<?php
namespace Home\Controller;
use Think\Controller;
class SupportController extends Controller {
    public function index(){
        $sup=M('support')->select();
        //dump($sup);
        $this->assign('sup',$sup);
        $this->display('index');
    }

    public function read($title){
        $title=str_replace(array("-","_")," ",$title);
        $where['title']=['eq',$title];
        $sup=M('support')->where($where)->find();
        $this->assign('title',$title);
        $this->assign('sup',$sup);
        //dump($sup);
        $this->display('read');
    }


}