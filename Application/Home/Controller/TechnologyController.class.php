<?php
namespace Home\Controller;
use Think\Controller;
class TechnologyController extends Controller {
    public function index(){

        //所有technology guide
        $map['title']=['neq','Laser Coders'];
        $techs=M('guide')->where($map)->select();
        $this->assign('techs',$techs);
        //dump($techs);
        //某一个technology guide
        $map['name']=['eq',$name];
        $tech=M('guide')->where($map)->select();
        $this->assign('tech',$tech);
        $this->display('index');
        
    }

    public function read($title){
        $title=str_replace(array("-","_")," ",$title);
        
        //所有technology guide
        $techs=M('guide')->select();
        $this->assign('techs',$techs);

        //某一个technology guide
        $map['title']=['eq',$title];
        $tech=M('guide')->where($map)->find();
        //dump($tech);
        $this->assign('tech',$tech);
        $this->assign('title',$title);
        $this->display('read');
    }


}