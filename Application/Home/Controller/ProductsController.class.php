<?php
namespace Home\Controller;
use Think\Controller;
class ProductsController extends Controller {
    public function index(){
        $products=M('products')->select();
        $this->assign('products',$products);
        $this->display('index');
    }

    public function read($name){
        $name=str_replace(array("-","_")," ",$name);
        //dump($name);
        $map['name']=array('eq',$name);
        $product=M('products')->where($map)->find();
        $products=M('products')->select();
        $where['cat_name']=['eq',$name];
        $goods=M('goods')->where($where)->select();
       
        //dump($goods);
        $this->assign('products',$products);
        $this->assign('goods',$goods);
        $this->assign('name',$name);
        $this->display('read');
    }

    public function show($cat_name,$name){
        //dump($cat_name);
        //dump($name);
        //die;
        $cat_name=str_replace(array("-","_",'#')," ",$cat_name);
        $name=str_replace(array("-","_")," ",$name);
        $where['name']=['eq',$name];
        
        $products=M('products')->select();
        $good=M('goods')->where($where)->find();
        $seo=$good['seo'];
        $seo=(json_decode($seo));
        $map['cat_name']=['eq',$cat_name];
        $goods=M('goods')->where($map)->select();
        //dump($products);
        //dump($goods);
        //seo词条
        $this->assign('cat_name',$cat_name);
        $this->assign('name',$name);
        $this->assign('seo',$seo);
        //产品分类
        $this->assign('products',$products);
        //某一个产品
        $this->assign('good',$good);
        $pics=M('pics')->where('gid='.$good['id'])->find();
        //dump($pics);
        $pics=explode(';',substr($pics['pics'],0,-1));
        //dump($pics);
        //die;
        $this->assign('pics',$pics);
        
        //某个分类下的产品
        $this->assign('goods',$goods);
        
        $this->display('show');

    }

    // public function detail(){

    // }


}