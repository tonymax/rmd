<?php
return array(
	//'配置项'=>'配置值'
    //数据库配置
    'DB_TYPE'               =>  'mysql',     // 数据库类型
    'DB_HOST'               =>  '127.0.0.1', // 服务器地址
    'DB_NAME'               =>  'rmd',          // 数据库名
    'DB_USER'               =>  'root',      // 用户名
    'DB_PWD'                =>  'root',          // 密码
    'DB_PORT'               =>  '3306',        // 端口

    //静态路由
	'URL_ROUTER_ON'   => true, 
	'URL_MAP_RULES'=>array(
		'news.html' => 'news/index.html',
		'Products.html'=>'Products/index.html',
		'http://rmd.me/list.php?catid=38' => 'http://rmd.me/News.html',
	),

	'HTML_CACHE_ON'     =>    true, // 开启静态缓存
	'HTML_CACHE_TIME'   =>    60,   // 全局静态缓存有效期（秒）
	'HTML_FILE_SUFFIX'  =>    '.php', // 设置静态缓存文件后缀
	'HTML_CACHE_RULES'  =>     array(  // 定义静态缓存规则
		// 定义格式1 数组方式
		'*'=>array('{$_SERVER.REQUEST_URI}'), 
		// 定义格式2 字符串方式
		//'静态地址'    =>     '静态规则', 
	),

	

	//路由自定义
	'URL_ROUTE_RULES'=>array(
		'news/:year/:month/:day' => array('News/archive', 'status=1'),
		//'news.html'		 =>'news/index.html',
		'news/read/:id'          => '/news/:1',
		'Products/:cat_name/:name' =>'Products/show',
		
		'news/:title'            =>'news/read',
		// 'news/:id'               => 'News/read',
		'Industries/:name'            => 'Industries/read',
        'Products/:name'	=> 'Products/read',
        'Technology/:title' => 'Technology/read',
        'Support/:title'    => 'Support/read',
        'About/:name'       => 'About/read',
		
	),
    
);