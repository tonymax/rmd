<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">



<html xmlns="http://www.w3.org/1999/xhtml">



<head>



    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />



    <title>Continuous Inkjet Printer,Pneumatic Coder,DOD Inkjet Printer</title>



    <meta content="Small Character Ink Jet Printer, Pneumatic Coder,DOD Inkjet Printer" name="keywords" />



    <meta content="Raymond inkjet printer can be used in a variety of metals, fonts , logos etc and widely applied to various industrial, food ,beverage etc.." name="description" />



    <base href="" />



    <link href="/Public/Home/css/style.css" rel="stylesheet" type="text/css" />

    <link href="/Public/Home/css/jquery.jslides.css" rel="stylesheet" type="text/css" />

    <!-- <link href="/Public/Home/css/demo.css" rel="stylesheet" type="text/css" /> -->

    <!-- <link href="/Public/Home/css/validform-style.css" rel="stylesheet" type="text/css" /> -->

    <!-- <script type="text/javascript" src="/Public/Home/js/jquery.min.js"></script> -->

    <script type="text/javascript" src="/Public/Home/js/jquery-1.8.0.min.js"></script>

    <script type="text/javascript" src="/Public/Home/js/jquery.jslides.js"></script>

    <script type="text/javascript" src="/Public/Home/js/jquery.SuperSlide.2.1.1.js"></script>

    <script type="text/javascript" src="/Public/Home/js/Validform_v5.3.2.js"></script>
    <!-- Smartsupp Live Chat script -->
<!-- <script type="text/javascript">
    var _smartsupp = _smartsupp || {};
    _smartsupp.key = 'befa92e2ed221f203d17940b0944cd7a6b69b113';
    window.smartsupp||(function(d) {
      var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
      s=d.getElementsByTagName('script')[0];c=d.createElement('script');
      c.type='text/javascript';c.charset='utf-8';c.async=true;
      c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
    })(document);
    </script> -->
    

</head>

<body>

    <!--顶部 -->
    
    <div class="top">
    
        <div class="wrap">
    
    
            <!--LOGO -->
    
    
            <div class="logo"><a href="http://www.whraymond.com/"><img src="/Public/Home/img/logo.png"/></a></div>
    
    
            <!--LOGO -->
    
    
            <div class="daohang">
                <ul>
                    <li><a href="/" class="now">Home</a></li>
                    <li><a href="/Industries.html" >Industries</a></li>
                    <li><a href="/Products.html" >Products</a></li>
                    <li><a href="/Technology.html" >Technology Guide</a></li>
                    <li><a href="/Support.html" >Support</a></li>
                    <li><a href="/Distributors.html" >Distributors</a></li>
                    <li><a href="/News.html" >News</a></li>
                    <li><a href="/About.html" >About</a></li>
                </ul>
            </div>
    
    
        </div>
    
    
    </div>
    
    
    <!--顶部 -->
    <style>#full-screen-slider {
        margin-top: 30px;
        margin-bottom: 30px;
    }</style>
    
    


<!--banner -->
<div id="full-screen-slider">
    <ul id="slides">
        <li style="background:url('/Public/Home/upload/20161128020323612.jpg') no-repeat center top"></li>
    </ul>
</div>
<!--banner -->

<!--盒子 -->
<div class="wrap">

    <!--左侧 -->
    <div class="hezi_left">

        <!--公司 -->
        <div class="gongsi">
            <div class="hz_bt"><a href="show.php?contentid=20">Company</a><span><a
                    href="show.php?contentid=20">More ></a></span></div>
            <div class="hz_nr">
                <span class="gs_pic"><a href="show.php?contentid=20"><img src="/Public/Home/img/ad/01.jpg"/></a></span>
                <span class="gs_dep"><a href="show.php?contentid=20">Wuhan Raymond Technology Co.,Ltd is the leading manufacturer of quality industrial marking equipment. We are committed to improving the quality of products and services by providing high quality and cost effective products that meet the needs of our customers. We specialize in laser....</a></span>
            </div>
        </div>
        <!--公司 -->


        <!--视频 -->
        <div class="shipin">
            <div class="hz_bt"><a href="/News">News</a><span><a href="list.php?catid=38">More ></a></span>
            </div>
            <div class="hz_nr">
                <ul>
                    <!-- <li><a href="News/How-to-choose-CIJ.html">How to choose CIJ</a></li> -->

                        <li><a href="News/1.html">How to choose CIJ</a></li><!-- <li><a href="News/Focus-shifts-to-exciting-flavors,-specialty-bars-in-chocolate-market.html">Focus shifts to exciting flavors, specialty bars in chocolate market</a></li> -->

                        <li><a href="News/2.html">Focus shifts to exciting flavors, specialty bars in chocolate market</a></li><!-- <li><a href="News/Plastic-pipe-demand-grows-for-construction,-water-infrastructure.html">Plastic pipe demand grows for construction, water infrastructure</a></li> -->

                        <li><a href="News/3.html">Plastic pipe demand grows for construction, water infrastructure</a></li><!-- <li><a href="News/Fruits-and-vegetables-account-for-34-percent-of-food-safety-violations.html">Fruits and vegetables account for 34 percent of food safety violations</a></li> -->

                        <li><a href="News/4.html">Fruits and vegetables account for 34 percent of food safety violations</a></li>                    
                    <!-- <li><a href="show.php?contentid=57">Focus shifts to exciting flavors, specialty bars in chocolate
                        market</a></li>
                    <li><a href="show.php?contentid=58">Plastic pipe demand grows for construction, water
                        infrastructure</a></li>
                    <li><a href="show.php?contentid=59">Fruits and vegetables account for 34 percent of food safety
                        violations</a></li> -->

                </ul>
            </div>
        </div>
        <!--视频 -->

        <div style="clear:both"></div>

        <!--产品 -->
        <div class="chanpin">
            <div class="hz_bt"><a href="Products.html">Products</a><span><a
                    href="Products.html">More ></a></span></div>
            <div class="picMarquee-left">
                <div class="bd">
                    <ul class="picList">
                        <li>
                                <div class="pic"><a href="Products/Continuous-Inkjet-Printer/A400.html"><img
                                        src="/Public/Home/upload/20161128030948418.jpg"/><span>A400</span></a></div>
                            </li><li>
                                <div class="pic"><a href="Products/Continuous-Inkjet-Printer/A300.html"><img
                                        src="/Public/Home/upload/20161129100027460.jpg"/><span>A300</span></a></div>
                            </li><li>
                                <div class="pic"><a href="Products/DOD-Inkjet-Printer/GL200P.html"><img
                                        src="/Public/Home/upload/20160101113300513.jpg"/><span>GL200P</span></a></div>
                            </li><li>
                                <div class="pic"><a href="Products/DOD-Inkjet-Printer/EBS250.html"><img
                                        src="/Public/Home/upload/20161129113100704.jpg"/><span>EBS250</span></a></div>
                            </li><li>
                                <div class="pic"><a href="Products/DOD-Inkjet-Printer/GL360.html"><img
                                        src="/Public/Home/upload/20160101113606218.jpg"/><span>GL360</span></a></div>
                            </li><li>
                                <div class="pic"><a href="Products/DOD-Inkjet-Printer/GL380.html"><img
                                        src="/Public/Home/upload/20160101113818220.jpg"/><span>GL380</span></a></div>
                            </li><li>
                                <div class="pic"><a href="Products/Thermal-Inkjet-Printer/TL200.html"><img
                                        src="/Public/Home/upload/20160101114154555.jpg"/><span>TL200</span></a></div>
                            </li><li>
                                <div class="pic"><a href="Products/Thermal-Inkjet-Printer/TL300.html"><img
                                        src="/Public/Home/upload/20160110094854984.jpg"/><span>TL300</span></a></div>
                            </li><li>
                                <div class="pic"><a href="Products/Laser-Coder/PL100.html"><img
                                        src="/Public/Home/upload/20160101120533178.jpg"/><span>PL100</span></a></div>
                            </li><li>
                                <div class="pic"><a href="Products/Laser-Coder/PL200.html"><img
                                        src="/Public/Home/upload/20160101120809434.jpg"/><span>PL200</span></a></div>
                            </li><li>
                                <div class="pic"><a href="Products/Pneumatic-Coder/SP1000.html"><img
                                        src="/Public/Home/upload/20160229045027541.jpg"/><span>SP1000</span></a></div>
                            </li><li>
                                <div class="pic"><a href="Products/Pneumatic-Coder/SP2000.html"><img
                                        src="/Public/Home/upload/20160101114302767.jpg"/><span>SP2000</span></a></div>
                            </li><li>
                                <div class="pic"><a href="Products/Consumable/Cleaner.html"><img
                                        src="/Public/Home/upload/20160131041611327.jpg"/><span>Cleaner</span></a></div>
                            </li><li>
                                <div class="pic"><a href="Products/Accessories/Accessories.html"><img
                                        src="/Public/Home/upload/20161202031116893.jpg"/><span>Accessories</span></a></div>
                            </li><li>
                                <div class="pic"><a href="Products/Accessories/.html"><img
                                        src="/Public/goods/2018-01-26/20180126_090417.jpg"/><span></span></a></div>
                            </li>                        <!-- <li>
                            <div class="pic"><a href="show.php?contentid=78"><img
                                    src="/Public/Home/upload/20160101114154555.jpg"/><span>TL200</span></a></div>
                        </li>
                        <li>
                            <div class="pic"><a href="show.php?contentid=69"><img
                                    src="/Public/Home/upload/20161128030948418.jpg"/><span>A400</span></a></div>
                        </li>
                        <li>
                            <div class="pic"><a href="show.php?contentid=68"><img
                                    src="/Public/Home/upload/20161129100027460.jpg"/><span>A300</span></a></div>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
        <!--产品 -->

        <script type="text/javascript">jQuery(".picMarquee-left").slide({
            mainCell: ".bd ul",
            autoPlay: true,
            effect: "leftMarquee",
            vis: 4,
            interTime: 50
        });</script>

    </div>
    <!--左侧 -->

    


<!--底部 -->

<!--右侧 -->
<div class="hezi_righr">

        <!--标题 -->
        <div class="r_bt">Contact US</div>
        <!--标题 -->

        <!--联系 -->
        <div class="lianxi">
            <span class="lx_tel">+86-27-86659195</span>
            <span class="lx_nr">
                <p>TEL：+86-27-86659195</p>
                <p>FAX：+86-27-86698193</p>
                <p>ADD：No 112,Guanggu Ave,Wuhan,China.Post code:430205</p>
                <p>MAIL：info@whraymond.com</p>
                <p>Contact：Andrew Yang</p>
            </span>

            <span class="lx_bot">
                <p class="sales">
                    <a href="/About/Contact-Sales.html">Contact Sales</a>
                </p>
            </span>


        </div>
        <!--联系 -->

    </div>
    <!--右侧 -->

</div>
<!--盒子 -->

<div style="clear:both"></div>

<div class="dibu">

    <ul class="wrap">

        <li>

            <dl>

                <dt>Coding Solutions</dt>
        
                <dd><a href="/Industries.html">Coated Metal</a></dd>

                <dd><a href="/Industries/Food.html" >Food</a></dd>

                <dd><a href="/Industries/Glass.html" >Glass</a></dd>

                <dd><a href="/Industries/Metal.html" >Metal</a></dd>

                <dd><a href="/Industries/Paper-and-card.html" >Paper and card</a></dd>

                <dd><a href="/Industries/Plastic.html" >Plastic</a></dd>

                <dd><a href="/Industries/Rubber.html" >Rubber</a></dd>

            </dl>

        </li>



        <li>

            <dl>

                <dt>Products</dt>

                <dd><a href="/Products.html">Continuous Inkjet Printer</a></dd>

                <dd><a href="/Products/DOD-Inkjet-Printer.html">DOD Inkjet Printer</a></dd>

                <dd><a href="/Products/Thermal-Inkjet-Printer.html">Thermal Inkjet Printer</a></dd>

                <dd><a href="/Products/Thermal-Transfer-Over-Printer.html">Thermal Transfer Over Printer</a></dd>

                <!--<dd><a href="/list.php?catid=75">Ink Roll Coder</a></dd>-->

                <dd><a href="/Products/Laser-Coder.html">Laser Coder</a></dd>

                <dd><a href="/Products/Pneumatic-Coder.html">Pneumatic Coder</a></dd>

                <dd><a href="/Products/Consumable.html">Consumable</a></dd>

                <dd><a href="/Products/Accessories.html">Accessories</a></dd>

            </dl>

        </li>



        <li>

            <dl>

                <dt>Technology Guide</dt>

                <!--<dd><a href="show.php?contentid=51">TTO</a></dd>-->

                <dd><a href="/Technology.html">Laser Coders</a></dd>

                <dd><a href="/Technology/TIJ.html">TIJ</a></dd>

                <dd><a href="/Technology/CIJ.html">CIJ</a></dd>

                <dd><a href="/Technology/DOD.html">DOD</a></dd>

                <dd><a href="/Technology/Pneumatic-marking-machine.html">Pneumatic marking machine</a></dd>



            </dl>

        </li>





        <li>

            <dl>

                <dt>Company</dt>

                <dd><a href="/About.html">About</a></dd>

                <dd><a href="/About/Event.html" >Event</a></dd>

                <dd><a href="/About/Certificate.html" >Certificate</a></dd>

                <dd><a href="/News.html">News</a></dd>

            </dl>

        </li>



    </ul>

</div>

<!--底部 -->



<!--底部 -->

<div class="footer">

    <div class="wrap">

<span class="footer_left">

<script type="text/javascript">

var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");

document.write(unescape("%3Cspan id='cnzz_stat_icon_1257578088'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s95.cnzz.com/z_stat.php%3Fid%3D1257578088%26show%3Dpic' type='text/javascript'%3E%3C/script%3E"));

</script>

<script>

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');



  ga('create', 'UA-74439664-1', 'auto');

  ga('send', 'pageview');



</script>

<p>
	Raymond Technology. All Rights Reserved.
</p></span>

        <span class="footer_right">

  <a href="https://www.youtube.com/channel/UCW1Ga2uRhC6GwSEHG4HJdOA"><img src="/Public/Home/img/05.jpg" /></a>

  <a href="https://plus.google.com/u/0/116533398269216120730"><img src="/Public/Home/img/01.jpg" /></a>

  <a href="https://www.facebook.com/injekpriint.raymond"><img src="/Public/Home/img/02.jpg" /></a>

  <a href="https://twitter.com/raymondinkjet"><img src="/Public/Home/img/03.jpg" /></a>

  <a href="http://www.linkedin.com/in/injekprinter-raymond-7b8a5a114?trk=nav_responsive_tab_profile_pic"><img src="/Public/Home/img/04.jpg" /></a>

</span>

    </div>

</div>

<!--底部 -->



</body>

</html>