<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">



<html xmlns="http://www.w3.org/1999/xhtml">



<head>



    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />



    <title>Continuous Inkjet Printer,Pneumatic Coder,DOD Inkjet Printer</title>



    <meta content="Small Character Ink Jet Printer, Pneumatic Coder,DOD Inkjet Printer" name="keywords" />



    <meta content="Raymond inkjet printer can be used in a variety of metals, fonts , logos etc and widely applied to various industrial, food ,beverage etc.." name="description" />



    <base href="" />



    <link href="/Public/Home/css/style.css" rel="stylesheet" type="text/css" />

    <link href="/Public/Home/css/jquery.jslides.css" rel="stylesheet" type="text/css" />

    <!-- <link href="/Public/Home/css/demo.css" rel="stylesheet" type="text/css" /> -->

    <!-- <link href="/Public/Home/css/validform-style.css" rel="stylesheet" type="text/css" /> -->

    <!-- <script type="text/javascript" src="/Public/Home/js/jquery.min.js"></script> -->

    <script type="text/javascript" src="/Public/Home/js/jquery-1.8.0.min.js"></script>

    <script type="text/javascript" src="/Public/Home/js/jquery.jslides.js"></script>

    <script type="text/javascript" src="/Public/Home/js/jquery.SuperSlide.2.1.1.js"></script>

    <script type="text/javascript" src="/Public/Home/js/Validform_v5.3.2.js"></script>
    <!-- Smartsupp Live Chat script -->
<!-- <script type="text/javascript">
    var _smartsupp = _smartsupp || {};
    _smartsupp.key = 'befa92e2ed221f203d17940b0944cd7a6b69b113';
    window.smartsupp||(function(d) {
      var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
      s=d.getElementsByTagName('script')[0];c=d.createElement('script');
      c.type='text/javascript';c.charset='utf-8';c.async=true;
      c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
    })(document);
    </script> -->
    

</head>

<body>

    <!--顶部 -->
    
    <div class="top">
    
        <div class="wrap">
    
    
            <!--LOGO -->
    
    
            <div class="logo"><a href="http://www.whraymond.com/"><img src="/Public/Home/img/logo.png"/></a></div>
    
    
            <!--LOGO -->
    
    
            <div class="daohang">
                <ul>
                    <li><a href="/" >Home</a></li>
                    <li><a href="/Industries.html" >Industries</a></li>
                    <li><a href="/Products.html" class="now">Products</a></li>
                    <li><a href="/Technology.html" >Technology Guide</a></li>
                    <li><a href="/Support.html" >Support</a></li>
                    <li><a href="/Distributors.html" >Distributors</a></li>
                    <li><a href="/News.html" >News</a></li>
                    <li><a href="/About.html" >About</a></li>
                </ul>
            </div>
    
    
        </div>
    
    
    </div>
    
    
    <!--顶部 -->
    <style>#full-screen-slider {
        margin-top: 30px;
        margin-bottom: 30px;
    }</style>
    
    



<!--位置 -->



<div class="weizhi wrap">
	<a href="http://www.whraymond.com/">Home</a> >
	<a href="/Products.html">Products</a> >
	<a href="Product/Continuous-Inkjet-Printer.html">Continuous Inkjet Printer</a> >
	<a href="Product/Continuous-Inkjet-Printer/A400.html">A400</a>
</div>



<!--位置 -->







<!--盒子 -->



<div class="wrap">







	<!--左侧 -->



	<div class="nav_left">







		<div class="dep_title">
			<h3>Products</h3>
		</div>



		<ul>







			<!--  文章所在栏目，及同级栏目循环 -->












			<li>
					<a href="/Products/Continuous-Inkjet-Printer.html">Continuous Inkjet Printer</a>
				</li>



				<!-- 开始调用同级文章循环 -->

				<dl class="pro_nav_cp">
					<dd>
								<a href="/Products/Continuous-Inkjet-Printer/A400.html">A400</a>
							</dd><dd>
								<a href="/Products/Continuous-Inkjet-Printer/A300.html">A300</a>
							</dd>				</dl><li>
					<a href="/Products/DOD-Inkjet-Printer.html">DOD Inkjet Printer</a>
				</li>



				<!-- 开始调用同级文章循环 -->

				<dl class="pro_nav_cp">
									</dl><li>
					<a href="/Products/Thermal-Inkjet-Printer.html">Thermal Inkjet Printer</a>
				</li>



				<!-- 开始调用同级文章循环 -->

				<dl class="pro_nav_cp">
									</dl><li>
					<a href="/Products/Laser-Coder.html">Laser Coder</a>
				</li>



				<!-- 开始调用同级文章循环 -->

				<dl class="pro_nav_cp">
									</dl><li>
					<a href="/Products/Pneumatic-Coder.html">Pneumatic Coder</a>
				</li>



				<!-- 开始调用同级文章循环 -->

				<dl class="pro_nav_cp">
									</dl><li>
					<a href="/Products/Consumable.html">Consumable</a>
				</li>



				<!-- 开始调用同级文章循环 -->

				<dl class="pro_nav_cp">
									</dl><li>
					<a href="/Products/Accessories.html">Accessories</a>
				</li>



				<!-- 开始调用同级文章循环 -->

				<dl class="pro_nav_cp">
									</dl>

			<!-- 开始调用同级文章循环 -->






			<!-- <li><a href="list.php?catid=72">DOD Inkjet Printer</a></li> -->



			<!-- 开始调用同级文章循环 -->



			<!-- <dl class="pro_nav_cp">






 </dl> -->



			<!-- 开始调用同级文章循环 -->






			<!-- <li><a href="list.php?catid=73">Thermal Inkjet Printer</a></li> -->



			<!-- 开始调用同级文章循环 -->



			<!-- <dl class="pro_nav_cp">






 </dl> -->



			<!-- 开始调用同级文章循环 -->






			<!-- <li><a href="list.php?catid=76">Laser Coder</a></li> -->



			<!-- 开始调用同级文章循环 -->



			<!-- <dl class="pro_nav_cp">






 </dl> -->



			<!-- 开始调用同级文章循环 -->






			<!-- <li><a href="list.php?catid=77">Pneumatic Coder</a></li> -->



			<!-- 开始调用同级文章循环 -->



			<!-- <dl class="pro_nav_cp">






 </dl> -->



			<!-- 开始调用同级文章循环 -->






			<!-- <li><a href="list.php?catid=82">Consumable</a></li> -->



			<!-- 开始调用同级文章循环 -->



			<!-- <dl class="pro_nav_cp">






 </dl> -->



			<!-- 开始调用同级文章循环 -->






			<!-- <li><a href="list.php?catid=83">Accessories</a></li> -->



			<!-- 开始调用同级文章循环 -->



			<!-- <dl class="pro_nav_cp">






 </dl> -->



			<!-- 开始调用同级文章循环 -->









			<!--  文章所在栏目，及同级栏目循环 -->



		</ul>







		<div class="nav_left_wz" style="margin-top:30px;">



			<h3>Become a Distributor</h3>



			<div class="selection">



				<span class="lx_bot">



					<p>
						<img src="/Public/Home/img/daili.jpg" width="138px" />
					</p>



					<p class="sales">
						<a href="/About/Contact-Sales.html">Fill in the form</a>
					</p>



				</span>



			</div>



		</div>







	</div>



	<!--左侧 -->







	<!--中间 -->



	<div class="nav_center">







		<div class="dep_bt">
			<h4>A400</h4>
		</div>







		<!--产品介绍 -->



		<div class="pro_box">







			<style>
				#full-screen-slider {
					width: 700px;
					height: 500px;
					float: left;
					position: relative;
					margin-bottom: 80px;
				}



				#slides {
					display: block;
					width: 700px;
					height: 500px;
					list-style: none;
					padding: 0;
					margin: 0;
					position: relative
				}



				#slides li {
					display: block;
					width: 700px;
					height: 500px;
					list-style: none;
					padding: 0;
					margin: 0;
					position: absolute
				}



				#slides li a {
					display: block;
					width: 700px;
					height: 500px;
					text-indent: -9999px
				}



				#pagination {
					margin-top: 30px;
					left: 0%;
					position: absolute;
				}



				#pagination li {
					float: left;
					display: block;
					width: 8px;
					height: 8px;
					margin-right: 15px;
					font-size: 12px;
					background: #666;
					text-align: center;
					border-radius: 10px;
					text-indent: -999em;
				}



				#pagination li a {
					color: #FFFFFF;
				}



				#pagination li.current {
					background: #0092CE
				}
			</style>














			<!--幻灯 -->



			<div class="pro_hd">



				<div id="full-screen-slider">



					<ul id="slides">




						<li style="background:url('/Uploads/2018-01-29/5a6ed7427f4db.jpg') no-repeat center top"></li><li style="background:url('/Uploads/2018-01-29/5a6ed74282b8c.jpg') no-repeat center top"></li><li style="background:url('/Uploads/2018-01-29/5a6ed74286625.jpg') no-repeat center top"></li>


						<!-- <li style="background:url('uploadfile/2016/1212/20161212110109190.jpg') no-repeat center top"></li>






<li style="background:url('uploadfile/2016/1212/20161212110109914.jpg') no-repeat center top"></li> -->






					</ul>



				</div>



			</div>



			<!--幻灯 -->






			<div style="clear:both"></div>







			<!--文字 -->



			<div class="nav_dep">



				<div class="product_nr">

					coding and marking solution. It is an affordable printer that requires minimal attention by operator and maintenance staff. The self-cleaning printhead and ruby nozzle, simple interface ,diaphragm pump ,accurate ink droplet dispersion technology which provide you more reliable ,clearer and easier ink jet coding experience.
Advance capabilities
1.Print up to 4 lines of text ,bar codes, 2D code ,and logos at speeds up to 300 meter/min.
2.USB connector ,allow the message transfer between different printers.
3.Optimized printhead heating thermostatic between different printers.
4.Optimized printhead heating thermostatic system,printhead temperature can be kept as a very stable level, more flexible to extreme environment .
Easier Operation and Maintenance
1. Simple interface with QWRTY keypad,easy to create and editing messages.
2. Automatic printhead cleaning at star up and shut down.
3. Separate design of the solenoid valves system makes maintenance easy and fast .
Printing Capability
1.Can print 1 to 4 lines of the print at speed up to 300 meter/min.
2.Font option : 5x5,5x7,7x9,11x9,11x11,24x16,32x24
3.Maximum print dot :34.
4.Character height:2-12mm
5.Print Distance:best distance 10mm,and range 2-10mm.
6.Current and expiration time,date coding and shift coding serializer
				</div>















			</div>



			<!--文字 -->















		</div>



		<!--产品介绍 -->















	</div>



	<!--中间 -->







	<!--右侧 -->



<div class="hezi_righr">







        <!--标题 -->
        
        
        
        <div class="r_bt">Contact US</div>
        
        
        
        <!--标题 -->
        <!--联系 -->
        <div class="lianxi">
        <span class="lx_tel">+86-27-86659195</span>
          <span class="lx_nr">
         <p>TEL：+86-27-86659195</p>
        <p>FAX：+86-27-86698193</p>
        <p>ADD：No 112,Guanggu Ave,Wuhan,China.Post code:430205</p>
        <p>MAIL：info@whraymond.com</p>
        <p>Contact：Andrew Yang</p>
          </span>
        
        <span class="lx_bot">
         <p class="sales"><a href="http://www.test.me/About/Contact-Sales.html">Contact Sales</a></p>
        </span>
        
        
        </div>
        <!--联系 -->
        
        
        
        
        
        
        
        
        
        
        
        <!--标题 -->
        
        
        
        <div class="r_bt" style="margin-top:30px;">Submit form</div>
        
        
        
        <!--标题 -->
        
        
        
        
        
        
        
        <!--表单 -->
        
        
        
        <div class="biaodan">
        
        
        
        <ul>
        
        
        
         
        
        
        
         <form name="myform" action="formguide/index.php" method="post" enctype="multipart/form-data">
        
        
        
         
        
        
        
        <li>
        
        
        
          <span>First name</span>
        
        
        
         <span><input type="text" name="info[first_name]" id="first_name" class="bdsr"/></span>
        
        
        
        </li>
        
        
        
        
        
        
        
        <li>
        
        
        
          <span>Last name</span>
        
        
        
         <span><input type="text" name="info[last_name]" id="last_name" class="bdsr"/></span>
        
        
        
        </li>
        
        
        
        
        
        
        
        <li>
        
        
        
          <span>Company name</span>
        
        
        
         <span><input type="text" name="info[company]" id="company" class="bdsr"/></span>
        
        
        
        </li>
        
        
        
        
        
        
        
        <li>
        
        
        
          <span>Email address</span>
        
        
        
         <span><input type="text" name="info[email]" id="email" class="bdsr"/></span>
        
        
        
        </li>
        
        
        
        
        
        
        
        <li>
        
        
        
          <span>Telephone</span>
        
        
        
         <span><input type="text" name="info[telephone]" id="telephone" class="bdsr"/></span>
        
        
        
        </li>
        
        
        
        
        
        
        
        
        
        
        
        <li>
        
        
        
          <span>Enquiry</span>
        
        
        
         <span><textarea name="info[enquiry]" id="enquiry"></textarea></span>
        
        
        
        </li>
        
        
        
        
        
        
        
        
        
        
        
        <li>
        
        
        
          <span>Country</span>
        
        
        
         <span><select name="info[country]" id="country"  size="1"  ><option value="Algeria" >Algeria</option>
        
        
        
        <option value="Argentina" >Argentina</option>
        
        
        
        <option value="Armenia" >Armenia</option>
        
        
        
        <option value="Australia" >Australia</option>
        
        
        
        <option value="Austria" >Austria</option>
        
        
        
        <option value="Bahamas" >Bahamas</option>
        
        
        
        <option value="Bahrain" >Bahrain</option>
        
        
        
        <option value="Bangladesh" >Bangladesh</option>
        
        
        
        <option value="Belarus" >Belarus</option>
        
        
        
        <option value="Belgium" >Belgium</option>
        
        
        
        <option value="Bermuda" >Bermuda</option>
        
        
        
        <option value="Bosnia and Hercegovina" >Bosnia and Hercegovina</option>
        
        
        
        <option value="Brazil" >Brazil</option>
        
        
        
        <option value="Bulgaria" >Bulgaria</option>
        
        
        
        <option value="Cameroon" >Cameroon</option>
        
        
        
        <option value="Canada" >Canada</option>
        
        
        
        <option value="Cayman Islands" >Cayman Islands</option>
        
        
        
        <option value="Chile" >Chile</option>
        
        
        
        <option value="China" >China</option>
        
        
        
        <option value="Colombia" >Colombia</option>
        
        
        
        <option value="Costa Rica" >Costa Rica</option>
        
        
        
        <option value="Croatia" >Croatia</option>
        
        
        
        <option value="Cyprus" >Cyprus</option>
        
        
        
        <option value="Czech Republic" >Czech Republic</option>
        
        
        
        <option value="Denmark" >Denmark</option>
        
        
        
        <option value="Dominican Republic" >Dominican Republic</option>
        
        
        
        <option value="Ecuador" >Ecuador</option>
        
        
        
        <option value="Egypt" >Egypt</option>
        
        
        
        <option value="El Salvador" >El Salvador</option>
        
        
        
        <option value="Estonia" >Estonia</option>
        
        
        
        <option value="Finland" >Finland</option>
        
        
        
        <option value="France" >France</option>
        
        
        
        <option value="Georgia" >Georgia</option>
        
        
        
        <option value="Germany" >Germany</option>
        
        
        
        <option value="Gibraltar" >Gibraltar</option>
        
        
        
        <option value="Global site" >Global site</option>
        
        
        
        <option value="Greece" >Greece</option>
        
        
        
        <option value="Hong Kong" >Hong Kong</option>
        
        
        
        <option value="Hungary" >Hungary</option>
        
        
        
        <option value="India" >India</option>
        
        
        
        <option value="Indonesia" >Indonesia</option>
        
        
        
        <option value="Iran" >Iran</option>
        
        
        
        <option value="Iraq" >Iraq</option>
        
        
        
        <option value="Ireland" >Ireland</option>
        
        
        
        <option value="Israel" >Israel</option>
        
        
        
        <option value="Italy" >Italy</option>
        
        
        
        <option value="Ivory Coast" >Ivory Coast</option>
        
        
        
        <option value="Japan" >Japan</option>
        
        
        
        <option value="Jordan" >Jordan</option>
        
        
        
        <option value="Kazakhstan" >Kazakhstan</option>
        
        
        
        <option value="Kenya" >Kenya</option>
        
        
        
        <option value="Korea" >Korea</option>
        
        
        
        <option value="Kuwait" >Kuwait</option>
        
        
        
        <option value="Latvia" >Latvia</option>
        
        
        
        <option value="Lebanon" >Lebanon</option>
        
        
        
        <option value="Libya" >Libya</option>
        
        
        
        <option value="Lithuania" >Lithuania</option>
        
        
        
        <option value="Luxembourg" >Luxembourg</option>
        
        
        
        <option value="Malawi" >Malawi</option>
        
        
        
        <option value="Malaysia" >Malaysia</option>
        
        
        
        <option value="Mauritius" >Mauritius</option>
        
        
        
        <option value="Mexico" >Mexico</option>
        
        
        
        <option value="Moldova" >Moldova</option>
        
        
        
        <option value="Montenegro" >Montenegro</option>
        
        
        
        <option value="Morocco" >Morocco</option>
        
        
        
        <option value="Nepal" >Nepal</option>
        
        
        
        <option value="Netherlands" >Netherlands</option>
        
        
        
        <option value="New Zealand" >New Zealand</option>
        
        
        
        <option value="Nigeria" >Nigeria</option>
        
        
        
        <option value="Norway" >Norway</option>
        
        
        
        <option value="Oman" >Oman</option>
        
        
        
        <option value="Pakistan" >Pakistan</option>
        
        
        
        <option value="Paraguay" >Paraguay</option>
        
        
        
        <option value="Peru" >Peru</option>
        
        
        
        <option value="Philippines" >Philippines</option>
        
        
        
        <option value="Poland" >Poland</option>
        
        
        
        <option value="Portugal" >Portugal</option>
        
        
        
        <option value="Puerto Rico" >Puerto Rico</option>
        
        
        
        <option value="Qatar" >Qatar</option>
        
        
        
        <option value="Romania" >Romania</option>
        
        
        
        <option value="Russia" >Russia</option>
        
        
        
        <option value="Saudi Arabia" >Saudi Arabia</option>
        
        
        
        <option value="Serbia" >Serbia</option>
        
        
        
        <option value="Singapore" >Singapore</option>
        
        
        
        <option value="Slovakia" >Slovakia</option>
        
        
        
        <option value="Slovenia" >Slovenia</option>
        
        
        
        <option value="South Africa" >South Africa</option>
        
        
        
        <option value="Spain" >Spain</option>
        
        
        
        <option value="Sri Lanka" >Sri Lanka</option>
        
        
        
        <option value="Sweden" >Sweden</option>
        
        
        
        <option value="Switzerland" >Switzerland</option>
        
        
        
        <option value="Syria" >Syria</option>
        
        
        
        <option value="Taiwan" >Taiwan</option>
        
        
        
        <option value="Tanzania" >Tanzania</option>
        
        
        
        <option value="Thailand" >Thailand</option>
        
        
        
        <option value="Tunisia" >Tunisia</option>
        
        
        
        <option value="Turkey" >Turkey</option>
        
        
        
        <option value="Uganda" >Uganda</option>
        
        
        
        <option value="Ukraine" >Ukraine</option>
        
        
        
        <option value="United Arab Emirates" >United Arab Emirates</option>
        
        
        
        <option value="United Kingdom" >United Kingdom</option>
        
        
        
        <option value="USA" >USA</option>
        
        
        
        <option value="Uzbekistan" >Uzbekistan</option>
        
        
        
        <option value="Venezuela" >Venezuela</option>
        
        
        
        <option value="Vietnam" >Vietnam</option>
        
        
        
        <option value="Zambia" >Zambia</option>
        
        
        
        <option value="Zimbabwe" >Zimbabwe</option>
        
        
        
        </select></span>
        
        
        
        </li>
        
        
        
        
        
        
        
        
        
        
        
        <li style="border:none;">
        
        
        
         <span><input type="hidden" name="dataid" value="" />
        
        
        
                <input type="hidden" name="formid" value="2" /><input type="submit" name="dosubmit"  value="Submit form" class="bdtj"/></span>
        
        
        
        </li>
        
        
        
        </form>
        
        
        
        
        
        
        
         </ul>
        
        
        
        </div>
        
        
        
        <!--表单 -->
        
        
        
        
        
        
        
        
        
        
        
        </div>
        
        
        
        <!--右侧 -->
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        </div>
        
        
        
        <!--盒子 -->
        
        
        
        <div style="clear:both"></div>
        
        <div class="dibu">

    <ul class="wrap">

        <li>

            <dl>

                <dt>Coding Solutions</dt>
        
                <dd><a href="http://www.test.me/Industries.html">Coated Metal</a></dd>

                <dd><a href="Industries/Food.html" >Food</a></dd>

                <dd><a href="Industries/Glass.html" >Glass</a></dd>

                <dd><a href="Industries/Metal.html" >Metal</a></dd>

                <dd><a href="Industries/Paper-and-card.html" >Paper and card</a></dd>

                <dd><a href="Industries/Plastic.html" >Plastic</a></dd>

                <dd><a href="Industries/Rubber.html" >Rubber</a></dd>

            </dl>

        </li>



        <li>

            <dl>

                <dt>Products</dt>

                <dd><a href="Products.html">Continuous Inkjet Printer</a></dd>

                <dd><a href="Products/DOD-Inkjet-Printer.html">DOD Inkjet Printer</a></dd>

                <dd><a href="Products/Thermal-Inkjet-Printer.html">Thermal Inkjet Printer</a></dd>

                <dd><a href="Products/Thermal-Transfer-Over-Printer.html">Thermal Transfer Over Printer</a></dd>

                <!--<dd><a href="/list.php?catid=75">Ink Roll Coder</a></dd>-->

                <dd><a href="Products/Laser-Coder.html">Laser Coder</a></dd>

                <dd><a href="Products/Pneumatic-Coder.html">Pneumatic Coder</a></dd>

                <dd><a href="Products/Consumable.html">Consumable</a></dd>

                <dd><a href="Products/Accessories.html">Accessories</a></dd>

            </dl>

        </li>



        <li>

            <dl>

                <dt>Technology Guide</dt>

                <!--<dd><a href="show.php?contentid=51">TTO</a></dd>-->

                <dd><a href="show.php?contentid=52">Laser Coders</a></dd>

                <dd><a href="show.php?contentid=54">TIJ</a></dd>

                <dd><a href="show.php?contentid=55">CIJ</a></dd>

                <dd><a href="show.php?contentid=66">DOD</a></dd>

                <dd><a href="show.php?contentid=67">Pneumatic marking machine</a></dd>



            </dl>

        </li>





        <li>

            <dl>

                <dt>Company</dt>

                <dd><a href="show.php?contentid=20">About</a></dd>

                <dd><a href="show.php?contentid=56" >Event</a></dd>

                <dd><a href="show.php?contentid=65" >Certificate</a></dd>

                <dd><a href="list.php?catid=38">News</a></dd>

            </dl>

        </li>



    </ul>

</div>

<!--底部 -->



<!--底部 -->

<div class="footer">

    <div class="wrap">

<span class="footer_left">

<script type="text/javascript">

var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");

document.write(unescape("%3Cspan id='cnzz_stat_icon_1257578088'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s95.cnzz.com/z_stat.php%3Fid%3D1257578088%26show%3Dpic' type='text/javascript'%3E%3C/script%3E"));

</script>

<script>

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');



  ga('create', 'UA-74439664-1', 'auto');

  ga('send', 'pageview');



</script>

<p>
	Raymond Technology. All Rights Reserved.
</p></span>

        <span class="footer_right">

  <a href="https://www.youtube.com/channel/UCW1Ga2uRhC6GwSEHG4HJdOA"><img src="/Public/img/05.jpg" /></a>

  <a href="https://plus.google.com/u/0/116533398269216120730"><img src="/Public/img/01.jpg" /></a>

  <a href="https://www.facebook.com/injekpriint.raymond"><img src="/Public/img/02.jpg" /></a>

  <a href="https://twitter.com/raymondinkjet"><img src="/Public/img/03.jpg" /></a>

  <a href="http://www.linkedin.com/in/injekprinter-raymond-7b8a5a114?trk=nav_responsive_tab_profile_pic"><img src="/Public/img/04.jpg" /></a>

</span>

    </div>

</div>

<!--底部 -->



</body>

</html>