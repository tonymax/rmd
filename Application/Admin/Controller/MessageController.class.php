<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Admin\Controller;
use User\Api\UserApi as UserApi;
use Admin\Model\AuthGroupModel;
use Think\Page;
use Think\Upload;

/**
 * 后台首页控制器
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
class MessageController extends AdminController {

    public function index()
    {
        $Msg=D('message');
        $msg=M('message')->select();

        $list=$this->lists($Msg,'create_time desc');

        $this->assign('_list',$list);
        $this->assign('msg',$msg);
        $this->display();
    }

    public function read($id){
        $msg=M('message')->find($id);
        $this->assign('msg',$msg);
        $this->display();
    }

    // public function add(){
    //     if(IS_POST){
    //         $rel=M('Message')->create();
    //         if($rel){
    //             M('Message')->save();
    //             $this->success('提交成功!');
    //         }else{
    //             $this->error('提交失败!');
    //         }
    //     }else{
    //         $this->display();
    //     }
    // }

}
