<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Admin\Controller;
use User\Api\UserApi as UserApi;
use Admin\Model\AuthGroupModel;
use Think\Page;

/**
 * 后台首页控制器
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
class NewsController extends AdminController {

    
    public function index(){
        $news=M('news')->select();
        $this->assign('news',$news);
        $this->display();
    }

    public function add(){
        if(IS_POST){
            $new=$_POST;
            $new['create_time']=time();
            if(M('news')->add($new)){
                $this->error('添加成功！',U('index'));
            }else{
                $this->error('添加失败！');
            }
        } else {
            
            $this->display();
        }
        
    }

    public function updateseo($id){
        $rel=M('News')->find($id);
        
        if($rel['seo']!=null){
            $seo=json_decode($rel['seo']);
            $title='编辑seo词条';
            $this->assign('seo',$seo);
            $this->assign('title',$tilte);
            $this->display('News/updateseo');
        }else{
            $title='还没添加关键字，请添加关键字';
            $this->assign('title',$title);
            $this->display('News/updateseo');
        }
        
    }

    public function read($id){
        
    }

    public function changeStatus($method,$id){
        if(IS_AJAX){
            $data['id']=$id;
            switch($method){
                case 'delete':
                    $data['status'] = 0;
                    $rel=M('News')->where('id ='.$id)->save($data);
                    if($rel){
                        $data['info']=$method.'数据成功！';
                    }else{
                        $data['info']=$method.'数据失败！';
                    }
                    $this->ajaxReturn($data);
                    $this->redirect('News/index');
                    break;
                case 'resume':
                    $data['status']=1;
                    $rel=M('News')->where('id='.$id)->save($data);
                    if($rel){
                        $data['info']=$method.'数据成功！';
                    }else{
                        $data['info']=$method.'数据失败！';
                    }
                    $this->ajaxReturn($data);
                    $this->redirect('News/index');
                    break;
            }
           
        }else{
            $this->display();
        }
    }

}
