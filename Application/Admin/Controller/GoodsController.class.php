<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Admin\Controller;
use User\Api\UserApi as UserApi;
use Admin\Model\AuthGroupModel;
use Think\Page;
use Think\Upload;

/**
 * 后台首页控制器
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
class GoodsController extends AdminController {

    
    public function index(){
        $Good=D('goods');
        $map=['status'=>1];
        $list=$this->lists('Goods',$map,'id asc');
        $goods=M('goods')->select();

        $this->assign('_list',$list);
        $this->assign('goods',$goods);
        $this->display();
    }

    public function add(){
        if(IS_POST){
            $good=$_POST;
            $good['create_time']=time();
            //dump($good);
            //die;
            //图片上传
            $upload = new \Think\Upload();// 实例化上传类
            $upload->maxSize   =     3145728 ;// 设置附件上传大小
            $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
            $upload->rootPath  =     './Public/'; // 设置附件上传根目录
            $upload->savePath  =     'goods/'; // 设置附件上传（子）目录
            $upload->saveName  =array('date','Ymd_his');
            $info   =   $upload->upload();
            $good['pic']=$info['pic']['savepath'].$info['pic']['savename'];
            $cat_name=M('products')->where('id ='.$good['cat_name'])->find();
             //dump($good);
             //dump($cat_name); 
             $good['cat_name']=$cat_name['name'];
             //dump($good);
             //die;

            if(!$info) {// 上传错误提示错误信息
                $this->error('上传失败！');
            }else{// 上传成功
                if(M('goods')->create($good)){
                    $result = M('goods')->add($good); // 写入数据到数据库 
                    if($result){
                        $this->success('添加成功！');
                    }else{
                        $this->error('添加失败！');
                    }
                }else{
                    $this->error('添加失败！');
                }
            }
            
        } else {
            $products=M('products')->select();
            $this->assign('products',$products);
            //dump($products);
            $this->display();
        }
        
    }

    public function addpic($id){
        if(IS_POST){
            //dump($id);
            //die;
            //图片上传；
            $upload = new \Think\Upload();
            $info=$upload->upload();
            //dump($info);
            $data['pics']='';
            foreach($info as $k=>$v){
               // dump($v);
                $data['pics'].=$v['savepath'].$v['savename'].';';
            }
            $data['gid']=$id;
            //dump($pic);
            //$data['id']=$id;
            // $data['id']=1;
            // $data['create_time']=time();
            //$data['pic_goods']=$pic;
            //dump($input);
            //$sql="UPDATE onethink_goods SET img='$pic' WHERE id =".$id;
            $rel=M('pics')->where('id='.$id)->add($data);
            //dump($rel);
            dump(M('pics')->getLastSql());
            //die;
            if($rel){
                $this->success('添加成功！',U('Goods/index'));
            }else{
                $this->error('添加失败！',U('Goods/index'));
            }

        }else{
            //dump($id);
            $this->assign('pic_id',$id);
            $this->display();
        }
    }

    public function changeStatus($method,$id){
        if(IS_AJAX){
            $data['id']=$id;
            switch($method){
                case 'delete':
                    $data['status'] = 0;
                    $rel=M('goods')->where('id ='.$id)->save($data);
                    if($rel){
                        $data['info']=$method.'数据成功！';
                    }else{
                        $data['info']=$method.'数据失败！';
                    }
                    $this->ajaxReturn($data);
                    $this->redirect('News/index');
                    break;
                case 'resume':
                    $data['status']=1;
                    $rel=M('goods')->where('id='.$id)->save($data);
                    if($rel){
                        $data['info']=$method.'数据成功！';
                    }else{
                        $data['info']=$method.'数据失败！';
                    }
                    $this->ajaxReturn($data);
                    $this->redirect('News/index');
                    break;
            }
           
        }else{
            $this->display();
        }
    }

}
